from dotenv 		  import load_dotenv
from flask            import Flask
from flask_cors       import CORS
from routes.cargamos  import cargamos_blueprint

import os

load_dotenv()

app = Flask(__name__)
app.register_blueprint(cargamos_blueprint)
CORS(app)

if __name__ == '__main__':
    app.run(threaded=True,host='0.0.0.0', port=8080, debug=True)