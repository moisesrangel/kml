from flask_migrate     import Migrate, MigrateCommand
from flask_script      import Manager, Command
from main              import app
from models.sqlalchemy import db
from models.sqlalchemy import Zone

import os

app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI')
db.init_app(app)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

class migrate(Command):
    'A command to add intial data'
    def run(self):
    	pass

if __name__ == '__main__':
    manager.run()
