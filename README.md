# KML

_Microservicio para agregar zonas con archivos KML y consultar la disponibilidad de un punto dentro de una zona__ 


### Pre-requisitos 📋

_Todas la librerias necesarias se enlistan en el archivo requirements_

> Python 3.6, pip
> Postgres Postgis (kartoza/postgis:12.0)

```
apt-get install python-pip
pip3 install -r requirements.txt
```

## Variables de entorno

_Es necesario tener un archivo .env con las siguientes variables_

```
	#Cadena de conexión en caso de que el adaptador sea sqlalchemy
	SQLALCHEMY_DATABASE_URI=postgresql+psycopg2://usuario:pwd@localhost:5432/cargamoskml
```

## Despliegue 📦

_Para levantar los datos de configuración iniciales_

```
psql -U cargamos -d cargamoskml -a -f zone.sql 
```

_Para levantar el entorno de desarrollo_

```
python3.6 manage.py runserver
```

## testing 📦

Las URLS disponibles:

* POST http://localhost:5000/cargamos/kml
* GET  http://localhost:5000/cargamos/kml (cliente web)
* GET  http://localhost:5000/cargamos/area/<latitude>/<longitude>

para correr las prubas

```
pytest
```






