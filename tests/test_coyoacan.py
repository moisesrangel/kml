import pytest
import requests
import os

class TestClass:
    def test_setkml(self):
        files  = {'file': open(os.getcwd() + '/tests/Coyoacan.kml', 'rb')}
        result = requests.post('http://localhost:5000/cargamos/kml', files=files)
        assert "200" in result.text

    def test_corazon_maguey(self):
        result = requests.get('http://localhost:5000/cargamos/area/-99.1657459/19.3496947')
        assert "Coyoacan" in result.text
        
    def test_bizarro(self):
        result = requests.get('http://localhost:5000/cargamos/area/-99.1651755/19.3511669')
        assert "Coyoacan" in result.text
        
    def test_polanco(self):
        result = requests.get('http://localhost:5000/cargamos/area/-99.2047097/19.4311971')
        assert "404" in result.text