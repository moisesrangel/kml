from flask              import Flask, flash, request, redirect, url_for
from flask              import Flask, request, Blueprint, jsonify, abort
from flask_cors         import CORS
from models.sqlalchemy  import * 
from os                 import path
from pykml              import parser
from sqlalchemy.sql     import text
from sqlalchemy.sql     import text
from werkzeug.utils     import secure_filename

import os

cargamos_blueprint = Blueprint('cargamos', __name__)
ALLOWED_EXTENSIONS = {'kml'}
UPLOAD_FOLDER      = os.getcwd()

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def read_kml(file):
    with open(file) as f:
        root = parser.parse(f).getroot()
        try:
            name        = str(root.Document.name)
            coordinates = str(root.Document.Placemark.Polygon.outerBoundaryIs.LinearRing.coordinates)
            wkt_polygon = coordinates.replace(',0','').replace(',', ' ')
            wkt_polygon = wkt_polygon[:-1]
            
            coordinates_list = list(filter(lambda x:x != '', list(map(lambda x:x.strip(), wkt_polygon.split('\n')))))
            the_geom = 'POLYGON(({}))'.format(','.join(coordinates_list))

            the_zone = Zone(name=name, boundary=the_geom)

            db.session.add(the_zone)
            db.session.commit()
            
            os.remove(file)

        except Exception as e:
            abort(400, 'Coordinates not found. ' + str(e)) 

@cargamos_blueprint.route('/cargamos/kml', methods = ['POST', 'GET'])
def upload_kml():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        
        file = request.files['file']
        
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(UPLOAD_FOLDER, filename)

            file.save(filepath)
            read_kml(filepath)
            
            return jsonify({ 'status' : 200 })

    return '''
    <!doctype html>
    <title>Upload KML File</title>
    <h1>Upload new KML File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''

@cargamos_blueprint.route('/cargamos/area/<latitude>/<longitude>', methods = ['GET'])
def get_area_by_point(latitude, longitude):
    with db.engine.connect() as con:
        statement = text("""SELECT name from public.zone where ST_Within(ST_Point(:p_latitude, :p_longitude), boundary);""")
        result    = con.execute(statement, { 'p_latitude' : latitude, 'p_longitude' : longitude })
        record    = result.fetchone()
        if record:
            return jsonify({ 'area' :  list(record)[0] })

        abort(404)

