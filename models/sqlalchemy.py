from flask            import Flask
from flask_migrate    import Migrate, MigrateCommand
from flask_script     import Manager
from flask_sqlalchemy import SQLAlchemy
#from geoalchemy2      import Geometry

#from sqlalchemy.ext.declarative import declared_attr

import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI')

db      = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = Manager(app)

#CREATE TABLE zone(id SERIAL PRIMARY KEY, name VARCHAR NOT NULL, boundary GEOMETRY NOT NULL);
#19.3540573,-99.178077 <- viveros de coyoacán (longitud/latitud)
#SELECT name from public.zone where ST_Within(ST_Point(-99.178077, 19.3540573), boundary);

class Zone(db.Model):
    id       = db.Column(db.Integer, primary_key=True)
    name     = db.Column(db.String(255), nullable=False)
    boundary = db.Column(db.Text(), nullable=False)

if __name__ == '__main__':
    manager.run()